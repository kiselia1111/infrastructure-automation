provider "aws" {
  region  = "us-west-2" # or any other region you're targeting
#   access_key = "YOUR_AWS_ACCESS_KEY"  # Uncomment and fill if you're using access/secret keys
#   secret_key = "YOUR_AWS_SECRET_KEY"
}


module "vpc_module" {
  source = "./modules/vpc"

  // If you wish, override the defaults
  cidr_block        = "10.0.0.0/16"
  subnet_cidr_block = "10.0.1.0/24"
  vpc_name          = "taks_2"
  subnet_name       = "taks_2"
}



module "alb" {
  source     = "./modules/alb_module"
  vpc_id     = module.vpc_module.vpc_id
  subnet_ids = module.vpc_module.subnet_ids
  ec2_instance_id = module.ec2_nginx.nginx_instance_id
}



module "ec2_nginx" {
  source             = "./modules/ec2_module"
  vpc_id             = module.vpc_module.vpc_id
  key_name           = "task_2"
  subnet_id          = module.vpc_module.subnet_ids
  security_group_ids = [module.alb.alb_security_group_id, module.ec2_nginx.ec2_ssh_security_group_id]
}


module "rds_postgres" {
  source        = "./modules/rds"
  vpc_id        = module.vpc_module.vpc_id
  subnet_ids    = module.vpc_module.subnet_ids
  db_username   = var.db_username
  db_password   = var.db_password
  ec2_sg_id     = module.ec2_nginx.ec2_ssh_security_group_id
}
