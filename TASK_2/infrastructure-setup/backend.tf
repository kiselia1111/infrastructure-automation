terraform {
  backend "s3" {
    bucket         = "task-2-terraform-state-bucket"
    key            = "TASK_2/infrastructure-setup/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "terraform-up-and-running-locks"
    encrypt        = true
  }
}
