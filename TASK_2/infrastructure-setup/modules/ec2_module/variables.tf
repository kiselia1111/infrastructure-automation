variable "ami_id" {
  description = "The ID of the Amazon Machine Image (AMI) to use for the instance."
  type        = string
  default     = "ami-03f65b8614a860c29"
}

variable "instance_type" {
  description = "The type of the EC2 instance."
  type        = string
  default     = "t2.micro"
}

variable "key_name" {
  description = "The key name to use for the instance."
  type        = string
}

variable "subnet_id" {
  description = "The VPC Subnet ID to launch in."
  type        = list(string)
}

variable "security_group_ids" {
  description = "The associated security groups in a list."
  type        = list(string)
}
variable "vpc_id" {
  description = "The VPC ID where the EC2 instance will be created."
  type        = string
}
