output "nginx_instance_id" {
  description = "ID of the EC2 instance running NGINX."
  value       = aws_instance.nginx_instance.id
}

output "ec2_ssh_security_group_id" {
  value = aws_security_group.ec2_ssh_sg.id
}
