variable "cidr_block" {
  description = "The CIDR block for the VPC."
  default     = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  description = "The CIDR block for the subnet."
  default     = "10.0.1.0/24"
}
variable "subnet_cidr_block_2" {
  description = "The CIDR block for the subnet."
  default     = "10.0.2.0/24"
}

variable "region" {
  description = "AWS region."
  default     = "us-west-2"
}

variable "vpc_name" {
  description = "Name tag for the VPC."
  default     = "task_2"
}

variable "subnet_name" {
  description = "Name tag for the subnet."
  default     = "subnet-task_2"
}
