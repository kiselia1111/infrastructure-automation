resource "aws_vpc" "my_vpc" {
  cidr_block       = var.cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = var.vpc_name
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = var.subnet_cidr_block
  availability_zone       = "${var.region}a" # By default, this creates a subnet in us-west-2a. You can customize this as needed.
  map_public_ip_on_launch = true

  tags = {
    Name = var.subnet_name
  }
}
resource "aws_subnet" "my_subnet_2" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = var.subnet_cidr_block_2
  map_public_ip_on_launch = true
  availability_zone       = "${var.region}b"  # Different AZ than the first subnet
  tags = {
    Name = "taks_2_subnet_2"
  }
}
resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "taks_2_igw"
  }
}

resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_igw.id
  }

  tags = {
    Name = "taks_2_route_table"
  }
}

resource "aws_route_table_association" "my_route_table_assoc_1" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.my_route_table.id
}

resource "aws_route_table_association" "my_route_table_assoc_2" {
  subnet_id      = aws_subnet.my_subnet_2.id
  route_table_id = aws_route_table.my_route_table.id
}
