output "vpc_id" {
  description = "The ID of the VPC."
  value       = aws_vpc.my_vpc.id
}

output "subnet_ids" {
  value = [aws_subnet.my_subnet.id, aws_subnet.my_subnet_2.id]
  description = "The IDs of the subnets."
}
