output "rds_endpoint" {
  description = "The connection endpoint for the RDS PostgreSQL instance"
  value       = aws_db_instance.myapp-postgres-db.endpoint
}

output "rds_security_group_id" {
  description = "The security group ID associated with the RDS instance."
  value       = aws_security_group.rds_postgres_sg.id
}
