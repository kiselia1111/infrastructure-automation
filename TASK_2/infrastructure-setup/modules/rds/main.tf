resource "aws_security_group" "rds_postgres_sg" {
  name        = "rds-postgres-sg"
  description = "Security group for RDS PostgreSQL"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    security_groups = [var.ec2_sg_id]# EC2 instance's security group is named ec2_ssh_sg.
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_db_subnet_group" "myapp_db_subnet_group" {
  name       = "myapp-db-subnet-group"
  subnet_ids = var.subnet_ids

  tags = {
    Name = "MyApp DB Subnet Group"
  }
}

resource "aws_db_instance" "myapp-postgres-db" {

  identifier = "myapp-postgres-db"
  engine     = "postgres"
  instance_class = "db.t3.micro"
  storage_type = "gp2"
  allocated_storage = 20
  storage_encrypted = true
  multi_az = true

  username = var.db_username
  password = var.db_password
  port     = "5432"

  vpc_security_group_ids = [aws_security_group.rds_postgres_sg.id]
  db_subnet_group_name = aws_db_subnet_group.myapp_db_subnet_group.name

  skip_final_snapshot = true # Set this to false in production. This is to delete the RDS instance without creating a snapshot.

  tags = {
    Name = "RDS Postgres Database"
  }
}
