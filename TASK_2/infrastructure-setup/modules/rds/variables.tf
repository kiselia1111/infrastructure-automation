variable "vpc_id" {
  description = "The VPC ID where RDS and security groups will be deployed."
  type        = string
}

variable "subnet_ids" {
  description = "List of subnet IDs for the RDS instance."
  type        = list(string)
}

variable "db_username" {
  description = "Username for the RDS database."
  type        = string
}

variable "db_password" {
  description = "Password for the RDS database."
  type        = string
  sensitive   = true
}

variable "ec2_sg_id" {
  description = "Security group ID of the EC2 instance."
  type        = string
}
