variable "vpc_id" {
  description = "The VPC ID for the ALB."
  type        = string
}

variable "subnet_ids" {
  description = "The subnet IDs for the ALB."
  type        = list(string)
}

variable "alb_name" {
  description = "The name for the ALB."
  type        = string
  default     = "my-alb"
}
variable "ec2_instance_id" {
  description = "ID of the EC2 instance to associate with the ALB"
  type        = string
}
