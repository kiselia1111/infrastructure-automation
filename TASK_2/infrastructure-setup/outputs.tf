output "nginx_instance_id" {
  value = module.ec2_nginx.nginx_instance_id
}
output "vpc_id" {
  value = module.vpc_module.vpc_id
}

output "subnet_id" {
  value = module.vpc_module.subnet_ids
}