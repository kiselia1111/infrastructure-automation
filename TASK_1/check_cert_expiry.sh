#!/bin/bash

# Check if domain argument is provided
if [[ -z "$1" ]]; then
    echo "Usage: $0 <domain>"
    exit 1
fi

# Define the target domain
DOMAIN=$1

# Check if the domain exists using nslookup
if ! nslookup $DOMAIN &>/dev/null; then
    echo "Domain $DOMAIN does not exist or is not resolvable."
    exit 2
fi

# Get the certificate info using OpenSSL
CERT_INFO=$(echo | openssl s_client -servername $DOMAIN -connect $DOMAIN:443 2>/dev/null | openssl x509 -noout -dates 2>/dev/null)

# Check if there's an error fetching the certificate
if [[ $? -ne 0 ]]; then
    echo "Failed to retrieve SSL/TLS certificate information for $DOMAIN."
    exit 1
fi

# Extract the expiration date
EXPIRY_DATE=$(echo "$CERT_INFO" | grep "notAfter" | cut -d= -f2)

# Check if the expiry date was retrieved successfully
if [[ -z "$EXPIRY_DATE" ]]; then
    echo "Failed to parse certificate information for $DOMAIN."
    exit 2
fi

# Display the result
echo "The SSL/TLS certificate for $DOMAIN expires on: $EXPIRY_DATE."

exit 0

