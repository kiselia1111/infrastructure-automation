# Senior DevOps Engineer - Technical Interview Tasks

## Task 1 - Certificate Expiry Date Checker Script
####Problem Statement:
Your task is to design and implement a script, which can be written in any scripting language of your choice (Python, Bash, PowerShell, etc.), to check and display the expiration date of the SSL/TLS certificate for the website "example.com." The script should perform the following steps:

1. Make an HTTPS request to "example.com" to retrieve the SSL/TLS certificate information.
2. Parse the certificate data to extract the expiration date.
3. Display the expiration date in a human-readable format.

####Script Requirements:

1. The script should be well-structured and maintainable, with appropriate comments and variable names for clarity.
2. Handle potential errors gracefully, such as network issues or invalid certificate data.
3. Ensure that the script provides a clear and readable output, making it easy to understand the certificate's expiration date.
4. Consider the efficiency of the script in terms of time and resource usage.

### Script Documentation
This script is used to check the SSL/TLS certificate expiration date for a given domain.

### External Libraries or Tools Used:
- **OpenSSL**: The script leverages the `openssl` tool to connect to the domain and retrieve its SSL/TLS certificate details.

- **nslookup**: The `nslookup` command is used to verify the existence and resolution of the provided domain.


Both openssl and nslookup are commonly available by default on many UNIX-like systems. If not, they can be installed through the system's package manager.

### Prerequisites:
Ensure you have openssl and nslookup installed on your system.

For Debian-based systems (like Ubuntu), you can install them using:
```bash
sudo apt-get install openssl dnsutils
```
For RedHat-based systems (like CentOS, Fedora):
```bash
sudo yum install openssl bind-utils
```
The script file check_cert_expiry.sh should have execute permissions. You can set it with:

```bash
chmod +x check_cert_expiry.sh
```
### How to Execute the Script:
Navigate to the directory containing the script.

Execute the script followed by the domain you wish to check:

```bash
./check_cert_expiry.sh example.com
```
Replace example.com with the domain for which you wish to check the SSL/TLS certificate expiration date.

Output:
The script will output the expiration date of the SSL/TLS certificate for the given domain, or provide error messages if there are any issues (e.g., domain not resolvable, unable to fetch certificate details).

##  Task 2 - Infrastructure Automation Setup
####Problem Statement:
This infrastructure automation challenge is designed to assess your ability to provision and manage cloud-based infrastructure using Infrastructure-as-Code (IaC)
principles.
You will create a public Git repository and set up a basic web application stack on a chosen Cloud Service Provider (e.g., AWS, Azure) using IaC tools like Terraform and Docker.
The stack should include a load balancer, web server, and a database running on separate instances or services.

####Script Requirements:

1. Create a public Git repository on a platform of your choice (e.g., GitHub, GitLab, Bitbucket).
2. Choose a Cloud Service Provider and Register an Account:
    * Select a free Cloud Service Provider (e.g., AWS, Azure, Google Cloud).
3. Automate Provisioning of the Application Stack:

    * Use Infrastructure-as-Code (IaC) tools such as Terraform, Docker, AWS CloudFormation, or Azure Resource Manager to automate the provisioning of the following components:
        * A load balancer.
        * Web server instances or containers (e.g., AWS EC2, Azure VMs, or AWS ECS).
        * A database of your choice (e.g., MySQL, PostgreSQL, or a managed service like AWS RDS or Azure Database).
    * Ensure that each component runs separately on virtual machines, containers, or as a managed service.

4. Implement automation for service fail-over, such as auto-restart of failing services.
    * Develop a script or use appropriate cloud-native features to detect service failures and initiate fail-over procedures.
    * Ensure that the fail-over process is well-documented in your repository.

5. Commit your IaC code, configuration files, and scripts to the Git repository.
    * Create clear and comprehensive documentation in the repository, including a README with setup instructions, explanations of code structure, and details on how manage the infrastructure.
      #####Presentation of Working Solution:

    * Prepare a live demo of your infrastructure automation solution. This should not be a presentation but a working demonstration of your provisioned environment.
    * Be ready to showcase how your solution handles fail-over scenarios.
    * 
# Infrastructure Setup with Terraform

This Terraform setup provisions the following AWS resources:

- **EC2 instance**: Used to run an Nginx server.
- **Elastic Load Balancer (ELB)**: Distributes incoming traffic across the EC2 instances.
- **RDS PostgreSQL Instance**: A multi-AZ relational database setup.
- **Security Groups**: Necessary security configurations for the above resources to communicate and to ensure proper access.

## Prerequisites

- **Terraform**: Ensure you have Terraform installed. If not, [download it here](https://www.terraform.io/downloads.html).
- **AWS CLI**: Set up and configured with the necessary access rights.
- **AWS Account**: Necessary to provision the resources.

## Setup and Deployment

1. **Initialize your Terraform environment**:
   ```bash
   terraform init
   ```
2. **Review the Execution Plan**:
  ```bash
   terraform plan
```
3. **Apply the Configuration:**:
  ```bash
   terraform apply
```
## Variables
- Ensure you provide the necessary variables. Here are some crucial ones:

- **vpc_id**: The VPC ID.
- **subnet_ids**: List of subnet IDs for the RDS instance.
- **db_username**: RDS database username.
- **db_password**: RDS database password.

Refer to the variables.tf file in each module for more details.